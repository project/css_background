<?php

namespace Drupal\css_background\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting CssBackground entities.
 *
 * @ingroup css_background
 */
class CssBackgroundEntityDeleteForm extends ContentEntityDeleteForm {
}
