<?php

namespace Drupal\css_background\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining CSS background type entities.
 */
interface CssBackgroundEntityTypeInterface extends ConfigEntityInterface {
}
