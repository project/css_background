<?php

namespace Drupal\css_background\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for CssBackground entities.
 */
class CssBackgroundEntityViewsData extends EntityViewsData {
}
