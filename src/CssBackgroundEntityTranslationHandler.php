<?php

namespace Drupal\css_background;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for product.
 */
class CssBackgroundEntityTranslationHandler extends ContentTranslationHandler {
}
